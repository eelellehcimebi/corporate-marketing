<details>
<summary>How to use this epic code</summary>

- Open a new tab and head to https://gitlab.com/groups/gitlab-com/-/epics/new
- Important that all child epics are at the same or lower level of project. Eg. if the parent epic is in marketing, simply make child epics there as well.
- Copy all of the info below `</details>`
- Paste it into the new epic draft box
- Click "create epic"

</details>

##### Organic Brand Social Media Epic for `INSERT EVENT NAME`

#### DRIs 
- Events team: `@handle here`
- Social team: `@handle here`

#### What is the story behind GitLab's creation or involvement in this event?

`insert narrative summary here` 

#### Core objectives of event

`insert objectives here`

#### Summary of important dates (estimate if not exact)

`insert summary of important dates here`

- CFP launch: `date`
- User reg launch: `date`
- event date(s): `date`

#### Campaign Details Needed
- Campaign UTM: `insert here`
- Finance code: `insert here`
- Slack channel for working group: `insert here`
- Agenda for ongoing meetings: `insert here`

#### Important social elements

- Hashtag?: `#InsertHere`
- Important handles/profiles?: `@inserthere` and `include links`

-----

<details>
<summary>Admin to create epic correctly</summary>

#### To Do
- [ ] Name is this epic: Social EVENT Epic
- [ ] Add this child epic to the parent event for all things related to this event

#### Once this epic is opened, please open one of each of the following issues [with this template](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/blob/master/.gitlab/issue_templates/social-event-owned-request.md)
- CFP Issue
- User Registration Issue
- Engagement / Coverage Issue for during event
- Post event promo

Be sure to:
- name and identify each issue for the reason above
- Count 4 issues related to this child epic

</details>

/label ~Events ~"Social Media" ~"Corp Comms" ~"Corporate Marketing"
