<!--
**Request-Rebranding-Issue-Template**

**STEP 1:** Read the instructions below.
**STEP 2:** Check the parent epic to ensure this is a new request.
**STEP 3:** Title this request: Rebranding Request - [ Asset(s) Name ]
**STEP 4:** Complete the rest of the request form.
**STEP 5:** Create the issue. We will be in touch!

Epic - https://gitlab.com/groups/gitlab-com/marketing/corporate_marketing/-/epics/197

**INSTRUCTIONS**

GitLab is preparing to reveal a brand refresh across all external-facing assets (public reveal end of April 2022). Elements that will be changing include a new font, type usage and weights, colors, and illustration style (ie logos).

So that we present a cohesive and complete presence to our customers, we need to know which assets need rebranding. To do that, we need your help in collecting the relevant templates, files, customer-facing content, websites, and social media that you know about and use in your role.

You do not need to submit a request for every asset or element you know of. For example, if you know you need to rebrand a number of assets in Marketo, please submit one request for Marketo rebranding and then provide details within the request about the known areas that contain branding elements. However, it is totally fine to submit a request for an individual asset if that asset isn't related to a larger asset.

Reach out to the DRIs [ @amittner @KBolton1 ] if you have any questions. Thank you!
-->

#### Brief description of the rebranding request
`Tell us what type of asset this is and summarize the rebranding needed (updated font, logo, etc).`

#### Where does the asset live?
* `(Link) to the current asset or location, if applicable`
* `(Link) to original issue, if applicable`

#### Was this asset originally created internally or by an agency/vendor?
* [ ] Internal
* [ ] Agency/Vendor
* [ ] I don't know

#### On a scale of LOW, MEDIUM, or HIGH, what is the priority level of your request?
* [ ] low
* [ ] medium
* [ ] high

`Please tell us why you chose that priority (high profile asset? several areas to update? Date sensitive?)`

#### Is there a specific due date this is needed by?
<!-- Note: The public reveal won't be until the end of April. -->
* [ ] YES <!-- Please enter the date in the Due Date field below -->
* [ ] FLEXIBLE

#### Additional notes
`Add any information that would be helpful in addressing your request.`

---

`** Brand Team Use ONLY **`

## Brand Review Status:
* [ ] Approved
* [ ] Reviewed with comment (comment in issue)

/label ~"design" ~"mktg-status::triage" ~"Corporate Marketing" ~"rebrand"

/assign @amittner @kbolton1

/epic gitlab-com/marketing/corporate_marketing&197

/confidential
