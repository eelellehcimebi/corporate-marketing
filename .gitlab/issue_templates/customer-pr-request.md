*Note: The issue should be created when a customer PR interest arises. Title format should be – Customer PR Support: Customer Name. Please insert details for each bullet point so PR team can determine if a story is press worthy.*

## :book: PR Team Customer Request Templates
* [Customer/Partner Media Request Template](https://docs.google.com/document/d/1xyI1r4YOxbU6A1Qp3tZeWZTTOgITIvtAbeF4jAvhEQk/edit)
* [Customer PR Intake Call Agenda Template](https://docs.google.com/document/d/1LokFzhRIjxWP5LZrQotdnL--jjo__yJxiAzrRBzDkT8/edit)
* [Customer PR Messaging Overview Template](https://docs.google.com/document/d/1mZolxg3_-Iz22JtWV-wT7BBBY6SPSwjvDTiLCg6KaT0/edit)

## :bulb: Opportunity
*Add details on the customer, how the opportunity came about and if it is related to any ongoing intiatives/launches*
* High level customer story themes and results: XX
* Customer Spokesperson: Name, Title
* Customer Comms Contact: Name, Title, Email Address, Phone Number 
* Customer Goal: XX

## :page_facing_up: Customer Story Information and Assets:
*Link any case studies, media articles, event presentations or assets relevant to the customer*
* XX

## :track_next: Next Steps
*Add additional actions as needed*
- [ ] Discuss storyline with customer
- [ ] Get approval from customer to pursue media opportunties 
- [ ] Conduct press outreach

## :dart: Goals 
*Describe ROI on this opportunity under both thought leadership and awareness subcategories.*
* Visibility: XX
* Thought leadership: XX
* Awareness: XX 

------

cc: @nwoods1 @cweaver1 @kpdoespr @nicolecsmith @steveg2022

/label ~"Corporate Marketing" ~"Corp Comms" ~"PR" ~"Thought Leadership" ~"Media Relations" ~"Executive Visibility" ~"customer story" 

/confidential
