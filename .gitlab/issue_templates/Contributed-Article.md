## Contributed Article Issue Template

- **Topic:**
- **Submission deadline (date, time, timezone):**
- **Is this for a specific publication/outlet?:** <!-- Yes/No -->
- **If so, which one? (include publication link):**
- **Estimated word count:** 
- **Who is the author/subject matter expert (SME)? Please include name + @gitlab handle, job title, and area of expertise:** 
- **High-res photo of author required?:** <!-- Yes/No; if Yes, please attach image file -->
- **Bio of author/subject matter expert:** 

## :notebook: **Abstract**

## :computer: **Resources**
- *Who: Who will be affected or will benefit from this article?*
- *What: What is the article about? What is the problem or solution described in the article?*
- *When (if applicable): When did it happen? When was the last update? When will the effects be felt? When can readers expect to learn more?*
- *Where: Where is this taking/where did it take place? Where should readers go to learn more?*
- *Why: Why did this happen? Why should readers care?*
- *How: How was this handled? If there was a problem, how was it solved? How will it impact the readers or the subject matter?*

## :calendar_spiral: **Timeline**

- [ ] Writer interviews SME (if applicable)
- [ ] @akulks drafts 
- [ ] SME reviews 
- [ ] @akulks updates
- [ ] Final draft is shared with SME
- [ ] SME approves
- [ ] Legal team reviews 
- [ ] Article sent for publication 


/label  ~"corporate marketing" ~"Contributed Articles" ~"PR" ~"Corp Comms" ~"mktg-status::plan" 

/assign @akulks

/confidential
