
**Re-Name this issue as [Trade Show] Staffing Issue

## :computer:  [TRADE SHOW] Staffing Overview & Mandatory Requirements to Attend:

**If you or your staff would like to participate onsite, please keep the following in mind:**

1. Are you and/or your employees allowed to travel for business?
1. Are you & your staff willing and able to travel to support the onsite GitLab booth per your assigned time?
1. Do you & your staff meet the shows [health + safety](INSERT WEBSITE LINK HERE) requirements?

## Next Steps 

**Once you have confirmed the above with your staff, each Manager will need each member of their staff to complete the checklist below. If you have not checked all the boxes by MMonth DDate Corporate Events cannot guarantee your spot.**

Copy and paste this section below into a comment thread _in this issue_ and tag **your manager**. All the checkboxes are required in order to attend the event.

**:flashlight: Eligibility**

 

If you have been assigned to attend and staff [TRADE SHOW]  on [DATES-DATES] in [CITY, STATE, COUNTRY] please confirm the following requirements have been completed:

- [  ] I am able to travel and have my managers approval.
- [  ] I agree to staffing the GitLab booth per my assigned times (approximately 4 hours/day for 3 days).
- [  ] I meet the show's health + safety requirements including full COVID-19 vaccination and mask mandate.
- [  ] I have booked my flight with Trip Actions.
- [  ] I have a hotel room booked through the show.
- [  ] I am fully registered for the event.
- [  ] I have reviewed the [Travel & Expense Guidelines](https://about.gitlab.com/handbook/finance/expenses/#-travel-and-expense-guidelines) for GitLab. 
- [  ] I agree to adhere to the [GitLab travel policy](https://about.gitlab.com/handbook/travel/#required-ofby-team-members-prior-to-traveling)

## :calendar: Onsite Schedule

**The staff schedule for the show will be created once we finalize who is approved to attend, speakers availability and other onsite activities that need to be staffed.**

:interrobang: For questions & to stay up to date, please join the [INSERT SLACK HANDLE] slack channel and our bi-monthly show planning call or contact @eparker9086.

Slack Handle: 





/label ~Events ~"Corporate Event" ~"Corporate Marketing" ~"In-Person Conference" ~"mktg-status::wip"
