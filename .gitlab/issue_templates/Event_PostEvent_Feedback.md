Rename this issue to: Feedback: In-person [TRADE SHOW YYYY] Staffing

## How you can help with this retro

Thank you so much for all of your time and support with our in-person activation at [TRADE SHOW], and we'd love to get your feedback to help us improve for the next event.

## How to organize feedback

:thumbsup: What went well?

- `Win 1`

- `Win 2`


:thumbsdown: What didn't go well?

- `Challenge 1`

- `Challenge 2`


:bulb: What can we improve?

- `Improvement 1`

- `Improvement 2` 


## Other helpful feedback to include

1. What should we do differently next time? 

1. What info do you wish you had before/during the project?

1. What should we have spent more time preparing?

1. Communication trials and tribulations

1. Staff numbers

1. Shift durations

-- Please leave the label below on this issue -->
/label ~Corporate Marketing ~Corporate Events
