<!-- Requestor, please open this issue, link the campaign/project issue and/or epic, and leave the rest empty. The social team will ask any further questions if needed. Thank you! -->

<!--Everything below this line is for the social team to fill out. -->

## Overview 
`EVENT OR CAMPAIGN NAME AND OVERVIEW`

## Goals
`PENDING`

## Perspective 
`PENDING`

## Suggested Posts

[Please follow the GitLab Team Member Social Media Policy and Guidelines when representing GitLab on your personal social media channels.](https://about.gitlab.com/handbook/marketing/social-media-guidelines/)

`ADD TABLE FOR POSTS HERE. FOR HELP, USE A GOOGLE SHEET TO CREATE YOUR TABLE AND COPY AND PASTE THE TABLE HERE. IT WILL APPEAR AS THE CODE FOR TABLES.`

_GitLab Team Members are not required to share, engage with, or comment on posts from GitLab or publish your own posts on your own channels. The Social + PR teams will not be acting in an oversight capacity, so there is no company reviewing of your tweets before you decide to publish them._

_If you would like to connect with a team member from social as a resource for any reason or if you'd like to keep up with GitLab posts throughout the week of events, consider joining the `#social_media_posts` Slack channel for an easy way to stay up to date on our latest posts._

<!-- Please leave the label below on this issue -->

/label ~"Social Media" ~"mktg-status::plan"

/assign @social

/milestone %"Organic Social: Triage"