# Request Info
### :page_with_curl: Request 

* Requested by: `@you`
* Due Date: `2021-01-01`

If it's an event, please provide the following details, remove this section otherwise:

* Event URL: `https://example.com`
* Location: `Virtual/Somewhere, Worldwide`
* Event Dates: `2021-01-01`

### :page_with_curl: Request Type
> Requests from other team members within GitLab.

* [ ]  Blog / Content Creation
* [ ]  Speaking Engagement (in-person)
* [ ]  Event Coverage (in-person)
* [ ]  Webcast
* [ ]  Media Coverage (interview, etc.)
* [ ]  CFP Submission Review
* [ ]  Content Review Request
* [ ]  Speaking Coaching
* [ ]  Other

### :speaking_head: Request Details 
> Add as many details as possible about the need.


### :building_construction: Request Triage Information
> This information will help the Developer Evangelism team triage the request and understand its impact when weighed against other requests in flight.

* Audience Description: `Who is this for?  What's the intended audience?`
* Expected Audience Size (live): `500`
* Expected Audience Size (post-creation online audience): `1200`
* Is this a new/nascent audience for GitLab (e.g. .NET developers)? `Yes/No`
* If this is a speaking session, will it be recorded and available for promotion? `Yes/No`


### :exclamation: Non-DevEvangelism Team Request Checklist (IMPORTANT)
This section is important if you from a different team within the company, it will help us triage your issue appropriately. > To help us track our [DE Budgets](https://about.gitlab.com/handbook/marketing/community-relations/developer-evangelism/workflow/index.html#request-budgets) please assign the right label that identifies the requesting team and a weight correlating with this issue's [budget score](https://about.gitlab.com/handbook/marketing/community-relations/developer-evangelism/workflow/index.html#scoring-requests) to allow us to track each team's budget consumption.


* [ ] `DE-Issue-Type::External`  assigned
* [ ] `DE-Type::Consulting` assigned
* [ ] Due Date added to issue.
* [ ] Selected the right option from the `DE-Consulting` scoped label options that represent the department the request belongs to
* [ ] Assigned the right Issue weight based on the type of request and your team/org, [learn more here](https://about.gitlab.com/handbook/marketing/community-relations/developer-evangelism/workflow/index.html#scoring-requests)
* [ ] A DRI has been assigned from the DevEvangelism Team or issue has been assigned to `@johncoghlan & @abuango` where no DRI is known yet.
* [ ] If the need is within the next week, please also slack the team in the [`#developer-evangelism`](https://app.slack.com/client/T02592416/CMELFQS4B) channel.


# Post Request Completion

## Request Completion Details

* Date of Execution/Completion: `<Date>`
* Link to resource: `<Video, Link, Doc, etc.>`
* Other Relevant Details: 

## Metrics

*Please provide any available metrics or accessible link to metrics*

## DE CheckList

* [ ] Added to [team calendar](https://about.gitlab.com/handbook/marketing/community-relations/developer-evangelism/#-team-calendar)

/label ~"dev-evangelism" ~"DE-Status::ToDo" ~"DE-Issue-Type::Internal" ~"DE-Type::Content" 

/assign @abuango @johncoghlan
